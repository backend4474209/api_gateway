package main

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"net"
	pb "server/genproto"
)

type Server struct {
	pb.UnimplementedAddServiceServer
}

func (s *Server) Add(ctx context.Context, request *pb.AddRequest) (*pb.AddResponse, error) {
	log.Println("received: ", request.GetX(), request.GetY())
	sum := request.GetX() + request.GetY()
	return &pb.AddResponse{
		Sum: sum,
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", "localhost:50051")
	if err != nil {
		log.Fatalln("error is while listen", err)
	}

	s := grpc.NewServer()
	pb.RegisterAddServiceServer(s, &Server{})

	log.Println(s.GetServiceInfo())
	if err := s.Serve(lis); err != nil {
		log.Fatalln("error is while serve", err)
	}
}
